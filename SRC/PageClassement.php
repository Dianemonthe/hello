<!DOCTYPE html>
<html lang=fr>
    <head>
    
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="PageClassement.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <div class="Fond2 p-3 mb-2"> 
        <header id="en-tête" class= "page header">
            <div class="row align-items-center">
                <div class="col-sm">
                     <p>
                        <img src="img.png" class="img-fluid" alt="ESIGELEC" id="logo" >
                    </p>
                </div>
                <div class="col-xs-8 col-md-6">
                <h1 class="text-center pt-4"> Classement des PING </h1>
                    <div class="row align-items-center justify-content-start">
                        
                    </div>
                </div>
                <div class="col-sm">
                    <div class="row align-items-center justify-content-end pr-5">
                        <div class="col-sm-4 text-center">    
                            <p>
                                <a href="http://localhost/SiteWeb/SRC_PagedAccueil.php" class="Connexion p-2 bg-light text-dark" >Deconnexion </a>
                            </p>
                        </div>
                    </div>        
                </div>
            </div>
        </header>
    </div>    
    </head>

    <body>
    <?php
        include 'bdd.inc.php';
        $bdd = new PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8',$user,$pass);
        $reponse = $bdd->query('SELECT * FROM posters ORDER BY nbrVote DESC');
        ?>
        
        <div class="container pt-3">    
            <div class="row justify-content-md-center ">
            <table>
    <thead>
        <tr>
            <th>Position</th>
            <th>Nom du Ping</th>
            <th>Numéro du PING</th>
            <th>Nombres de votes</th>
        </tr>
    </thead>
    <?php
     $a=1;
    while ( $donnees = $reponse -> fetch()){
    ?>
    <tbody>
        <tr>
            <th> <?php echo $a++; ?> </th>
            <th> <?php echo $donnees['nomPing']; ?> </th>
            <th> <?php echo $donnees['NumeroPing']; ?> </th>
            <th> <?php echo $donnees['nbrVote']; ?> </th>
            
        </tr>
        
        
    </tbody>
    <?php
    }
    $reponse->closeCursor();
    ?>
</table>
            </div>
        </div>
    </body>
</html>