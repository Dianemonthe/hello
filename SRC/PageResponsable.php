<!DOCTYPE html>
<html lang=fr>
    <head>
    
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <div class="Fond1 p-3 mb-2" > 
        <header id="en-tête" class= "page header">
            <div class="row align-items-center">
                <div class="col-sm">
                     <p>
                        <img src="images/esigelecLogo.png" class="logo img-fluid" alt="ESIGELEC" id="logo" >
                    </p>
                </div>
                <div class="col-xs-8 col-md-6">
                <h1 class="text-center col-pt-4"> Bienvenue Nezim </h1>
                </div>
                <div class="col-sm">
              
                    <div class="row align-items-center justify-content-end pr-5">
                    <div class="col-sm-4 text-center">    
                            <p>
                                <a href="http://localhost/SiteWeb/SRC_PagedAccueil.php" class="Connexion p-2 bg-light text-dark" >Deconnexion </a>
                            </p>
                        </div>
            
                     
                    </div>        
                </div>
            </div>
        </header>
    </div>    
    </head>

    <body>
    <div class="container-fluid pink">

    <div class="row align-items-center justify-content-end pr-3">
            
                     
            
                        <div class="col-sm-2 text-center">    
                            <p>
                                <a href="http://localhost/SiteWeb/PageAjout.php" class="AjouterUnProjet p-2 bg-light text-dark" >Ajouter Projet </a>
                            </p>
                                </div>
                            <div class="col-sm-2 text-center">    
                                <p>
                                <a href="http://localhost/SiteWeb/PageGerer.php?" class="GérerlesVotes p-2 bg-light text-dark" >Gérer les votes </a>
                                </p>
                             </div>
                        <div class="col-sm-2 text-center">    
                            <p>
                                <a href="http://localhost/SiteWeb/PageClassement.php" class="Classement p-2 bg-light text-dark" >Voir Classement </a>
                            </p>
    </div> 
                   
        <footer>
        <div class="input-group">
  <select class="custom-select" id="inputGroupSelect04" aria-label="Example select with button addon">
    <option selected>Choissisez un poster</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
  </select>
  <div class="input-group-append">
  <div class="text-center"><button type="submit" class="btn btn-primary center-block mb-3">Voter</button></div>
  </div>
</div>
</footer>
</div>
</body>

</html>