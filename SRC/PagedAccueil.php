<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>
       Page d'accueil
    </title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="container-fluid pink">
      <div class="row">
        <div class="col-5">
          <p class="positionlogo">
          <img src="images/esigelecLogo.png" alt="logo de l'ESIGELEC" class="img-thumbnail" width="400" height="150"></p>
        </div>
  </div>
    <div class="row justify-content-around">
      <div class="col-6">
        <p class="titrepingnd"> PING ND </p>
      </div>
      <div class="col-md-5">
        <p class="messageconnexion"> Connectez vous pour découvrir les projets ou Inscrivez vous!  </p>
      </div>
    </div>

    <div class="row justify-content-around">
       <div class="col-6">
        <p class="texteping">
        Le PING est un projet INDUSTRIEL réalisé par des élèves de l'ESIGELEC. Venez découvrir les différents projets à travers des posters.
                   Un vote désignera le meilleur projet. Vous pouvez y participez, à vous de jouer!</p>
       </div>

       <form action="TraitementConnexion.php" method="post">
        <div class="form-group">
         <div class="col-md-16">
           <p class="positionlabel">
           <label for="formGroupExampleInput"><strong>Identifiant: </strong></label>
           <input type="text" name="identifiant" required class="form-control form-control-sm" id="formGroupExampleInput" placeholder=""></p>
         </div>
        </div>

        <div class="form-group">
         <div class="col-md-14">
           <p class="positionlabel">
           <label for="inputPassword5"><strong>Mot de passe: </strong></label>
           <input type="password" id="inputPassword5" class="form-control form-control-sm" aria-describedby="passwordHelpBlock"></p>
           <small id="passwordHelpBlock" name="motDePasse" required class="form-text text-muted"></small>
         </div>
        </div>

        <div class="row justify-content-around">
            <div class="col-auto">
              <p class="positionboutonb">
              <button type="button" name="connexion" required class="btn btn-primary btn-sm">Connexion</button></p>
            </div>
        </div>

        <div class="row justify-content-end">
            <div class="col-6">
              <p class="positioninscriptiona"><strong> Nouveau? </strong></p>
            </div>

            <div class="col-6">
              <p class="positioninscriptionb">
              <button type="button" class="btn btn-primary btn-sm"> <a href="PagedInscription.php"</a> Inscription </button></p>
            </div>

       </div>
</form>
   </div>




     </div>

  </body>
</html>
