<?php
    session_start();
    include('connexionBDD.php'); //il manque db/

    if (isset($_SESSION['id'])){
        header('Location: PagedAccueil.php');
        exit;
    }
    
    // Si la variable "$_Post" contient des informations alors on les traitres
    if(!empty($_POST)){
    extract($_POST);
    $valid = true;
    }

    if (isset($_POST['sinscrire'])){
        $nom = htmlentities(trim($nom)); // On r?up?e le nom
        $prenom = htmlentities(trim($prenom)); // on r?up?e le pr?om
        $identifiant = htmlentities(strtolower(trim($identifiant))); // On r?up?e le mail
        $motDePasse= trim($motDePasse); // On r?up?e le mot de passe?

        // V?ification du nom
        if(empty($nom)){
            $valid = false;
            $er_nom = ("Le nom d' utilisateur ne peut pas ?re vide");
        }

        //?V?ification du pr?om
        if(empty($prenom)){
            $valid = false;
            $er_prenom = ("Le prenom d' utilisateur ne peut pas ?re vide");
        }

        // V?ification du mail
        if(empty($identifiant)){
            $valid = false;
            $er_identifiant = "Le mail ne peut pas ?re vide";
        }

        elseif(!preg_match("/^[a-z0-9\-_.]+@[a-z]+\.[a-z]{2,3}$/i", $identifiant)){
            $valid = false;
            $er_identifiant = "Le mail n'est pas valide";
        }
        else{
            // On v?ifit que le mail est disponible
            $req_mail = $BDD->query("SELECT identifiant FROM utilisateurs WHERE identifiant = ?",
                array($identifiant));

            $req_mail = $req_mail->fetch();

             if ($req_mail['identifiant'] <> ""){
                $valid = false;
                $er_mail = "Ce mail existe d??";
            }
        }

        // V?ification du mot de passe

        if(empty($motDePasse)) {
           
            $valid = false;
            $er_motDePasse = "Le mot de passe ne peut pas ?re vide";
        }

        // Si toutes les conditions sont remplies alors on fait le traitement
       // var_dump($valid); exit;
        if($valid){

            $motDePasse = md5($motDePasse);
            $date_creation_compte = date('Y-m-d H:i:s');

            // On insert nos donn?s dans la table utilisateur
            $BDD->insert("INSERT INTO utilisateurs (role, groupe, nom, prenom, identifiant, motDePasse) VALUES
            (1, 3, ?, ?, ?, ?)",
            array($nom, $prenom, $identifiant, $motDePasse));

            header('Location: PagedInscription.php');

            exit;
        }
    }

?>
