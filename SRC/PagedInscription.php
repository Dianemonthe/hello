<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>
      Login - PING Projet
    </title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="container-fluid pink">
      <div class="row justify-content-between">
        <div class="col-5">
          <p class="positionlogoConnexion">
          <img src="images/esigelecLogo.png" alt="logo de l'ESIGELEC" class="img-thumbnail" width="400" height="150"></p>
        </div>

        <div class="col-4">
          <p class="positionlogologin">
          <img src="images/inscriptionLogo.png" alt="logo de l'ESIGELEC" class="img-thumbnail" width="100" height="70"></p>
        </div>
     </div>

     <div class="row align-self-center">
       <div class="col-md-12">
         <p class="titrepageConnexion"> PING ND </p>
       </div>
    </div>

  <h4> <strong>Créer un compte </strong> </h4>

    <form action="TraitementInscription.php" method="post">
     <div class="form-group">
       <div class="col-11">
         <p class="positionlabelPagConnexion">
         <label for="exampleInput"> <strong> Nom: </strong></label>

         <input type="text" name="nom" required class="form-control" id="formGroupExampleInput" placeholder=""> </p>
     </div>
     <div class="form-group">
       <div class="col-11">
         <p class="positionlabelPagConnexion">
         <label for="exampleInput"> <strong> Prénom: </strong> </label>

         <input type="text" name="prenom" required class="form-control" id="formGroupExampleInput" placeholder=""> </p>
     </div>
     <div class="form-group">
       <div class="col-11">
         <p class="positionlabelPagConnexion">
         <label for="exampleInputEmail1"><strong>Identifiant: </strong></label>

         <input type="email" name="identifiant" required class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="prenom.nom@groupe-esigelec.org"></p>
     </div>
     <div class="form-group">
       <div class="col-11">
       <p class="positionlabelPagConnexion">
       <label for="exampleInputPassword1"> <strong>Mot de passe: </strong></label>

       <input type="password" name="motDePasse" required class="form-control" id="exampleInputPassword1" placeholder=""></p>
     </div>

     <div class="form-group">
       <div class="col-11">
       <p class="positionlabelPagConnexion">
       <label for="exampleInputPassword1"> <strong>Numéro PING: </strong></label>

       <input type="password" name="numeroPing" required class="form-control" id="exampleInputPassword1" placeholder=""></p>
     </div>

     <p class="positionsinscrirebtn">
     <button type="submit" name="sinscrire" class="btn btn-primary btn-sm">S'INSCRIRE</button></p>
    </div>
    </div>
   </div>
   </form>
  </body>
</html>
