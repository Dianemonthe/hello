<!DOCTYPE html>
<html lang=fr>
    <head>
    
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="PageGerer.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <div class="Fond p-3 mb-2"> 
        <header id="en-tête" class= "page header">
            <div class="row align-items-center">
                <div class="col-sm">
                     <p>
                        <img src="img.png" class="img-fluid" alt="ESIGELEC" id="logo" >
                    </p>
                </div>
                <div class="col-xs-8 col-md-6">
                <h1 class="text-center pt-4"> Gérer les Votes </h1>
                    <div class="row align-items-center justify-content-start">
                       
                    </div>
                </div>
                <div class="col-sm">
                    <div class="row align-items-center justify-content-end pr-5">
                        <div class="col-sm-4 text-center">    
                            <p>
                                <a href="http://localhost/SiteWeb/SRC_PagedAccueil.php" class="Connexion p-2 bg-light text-dark" >Deconnexion </a>
                            </p>
                        </div>
                    </div>        
                </div>
            </div>
        </header>
    </div>    
    </head>

    <body>
        
        <div class="container pt-3">    
            <div class="row justify-content-md-center ">
                <div class="col-sm-5">
                    <div class="col-sm align-self-center">
                        <form>
                            <div class="form-group">
                                <label for="InputPING">L'élection est actuellemnt fermée.
                    
                                </label>
                                
                                <input type="number" class="form-control" id="Temps d'élection" placeholder="Temps d'élection">
                            </div>
                            
                            <div class="text-center"><button type="submit" class="btn btn-primary center-block mb-3">Démarrer l'Élection</button></div>

                             <div class="text-center"><button type="submit" class="btn btn-primary center-block mb-3">Arrêter l'Élection</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>