<!DOCTYPE html>
<html lang=fr>
    <head>
    
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="PageAccueil.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <div class="Fond p-3 mb-2"> 
        <header id="en-tête" class= "page header">
            <div class="row align-items-center">
                <div class="col-sm">
                     <p>
                        <img src="img.png" class="img-fluid" alt="ESIGELEC" id="logo" >
                    </p>
                </div>
                <div class="col-xs-8 col-md-6">
                <h1 class="text-center pt-4"> </h1>
                    <div class="row align-items-center justify-content-start">
                       
                    </div>
                </div>
                
            </div>
        </header>
    </div>    
    </head>
  <body>
    <div class="container-fluid pink">
      <div class="row">
        
  </div>
    <div class="row justify-content-around">
      <div class="col-6">
        <p class="titrepingnd"> PING ND </p>
      </div>
      <div class="col-md-5">
        <p class="messageconnexion"> Connectez vous pour découvrir les projets ou Inscrivez vous!  </p>
      </div>
    </div>

    <div class="row justify-content-around">
       <div class="col-6">
        <p class="texteping">
        Le PING est un projet INDUSTRIEL réalisé par des élèves de l'ESIGELEC. Venez découvrir les différents projets à travers des posters.
                   Un vote désignera le meilleur projet. Vous pouvez y participez, à vous de jouer!</p>
       </div>

       <form action="bddConnect.php" method="POST">
        <div class="form-group">
         <div class="col-md-16">
           <p class="positionlabel">
           <label for="formGroupExampleInput"><strong>Identifiant: </strong></label>
           <input type="text" name="id" class="form-control form-control-sm" id="formGroupExampleInput" placeholder=""></p>
         </div>
        </div>

        <div class="form-group">
         <div class="col-md-14">
           <p class="positionlabel">
           <label for="inputPassword5"><strong>Mot de passe: </strong></label>
           <input type="password" name="pass" id="inputPassword5" class="form-control form-control-sm" aria-describedby="passwordHelpBlock"></p>
           <small id="passwordHelpBlock" class="form-text text-muted"></small>
         </div>
        </div>

        <div class="row justify-content-around">
            <div class="col-auto">
              <p class="positionboutonb">
              <button type="submit" class="btn btn-primary btn-sm">Connexion</button></p>
            </div>
        </div>

        <div class="row justify-content-end">
            <div class="col-6">
              <p class="positioninscriptiona"><strong> Nouveau? </strong></p>
            </div>

            <div class="col-6">
              <p class="positioninscriptionb">
              <button type="button" class="btn btn-primary btn-sm"> <a href="SRC_PagedInscription.php"> 
              </a> Inscription </button></p>
            </div>

       </div>
       
</form>
   </div>




     </div>

  </body>
</html>
