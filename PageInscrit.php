<?php  session_start();

if(isset($_POST['poster'])) {
    $posterChoisi = $_POST['poster']; 
    $_SESSION['posterChoisi'] = $posterChoisi;
}

 ?>
<!DOCTYPE html>
<html lang=fr>
    <head>
    
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="PageInscrit.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <div class="Fond1 p-3 mb-2" > 
        <header id="en-tête" class= "page header">
            <div class="row align-items-center">
                <div class="col-sm">
                     <p>
                        <img src="images/esigelecLogo.png" class="logo img-fluid" alt="ESIGELEC" id="logo" >
                    </p>
                </div>
                <div class="col-xs-8 col-md-6">
                <!--<form action="TraitementInscription.php" method="post">-->
                <h1 class="text-center col-pt-4"> Bienvenue Nezim </h1>
                </div>
                <div class="col-sm">
              
                    <div class="row align-items-center justify-content-end pr-5">
                    <div class="col-sm-4 text-center">    
                            <p>
                                <a href="http://localhost/SiteWeb/SRC_PagedAccueil.php" class="Connexion p-2 bg-light text-dark" >Deconnexion </a>
                            </p>
                        </div>
            
                     
                    </div>        
                </div>
            </div>
        </header>
    </div>    
    </head>

    <body>
    <?php

        //var_dump($_SESSION); exit;
     ?>

    <div class="container-fluid pink">

    <div class="row align-items-center justify-content-end pr-3">
    <div class="col-sm-2 text-center">    
                            <p>
                                <a href="http://localhost/SiteWeb/PageClassement.php" class="Classement p-2 bg-light text-dark" >Voir Classement </a>
                            </p>
    </div> 
               
               <?php //var_dump($_POST); exit; ?>    
        <footer>
        <div class="input-group">
            <form action="" method="POST">
                <select class="custom-select" id="inputGroupSelect04" name= "poster" aria-label="Example select with button addon">
                    
                     <?php if(empty($_SESSION['posterChoisi'])) { ?>

                         <option selected>Choissisez un poster</option>
                         <option value="1">1</option>
                     <option value="2">2</option>
                     <option value="3">3</option>
                                    <?php     }else { ?>
                                             <option selected>Vous avez choisi le poster <?php echo $posterChoisi; ?></option>

                                        <?php } ?>
                    
                </select>
                <div class="input-group-append">
                <div class="text-center"><button type="submit" class="btn btn-primary center-block mb-3">Voter</button></div>
  </div>
            </form>
  
  

</div>

<div class="row justify-content-around"> 

  <div class="col-sm-4 text-center">
    <?php if(isset($_SESSION['posterChoisi']) && $_SESSION['posterChoisi'] == 1){ ?>
    <b><u>Ceci est votre choix</u></b>

     
     <?php } else{?>
        <b>poster 1</b>
        
    <?php } ?>
  <p>
     <img src="images/Poster1.jpg" class="logo img-fluid" alt="poster1" id="logo" >
  </p>
</div>

 <div class="col-sm-4 text-center">
     <b>poster 2</b>
  <p>
     <img src="images/Poster2.jpg" class="logo img-fluid" alt="poster2" id="logo" >
  </p>
</div>

  <div class="col-sm-4 text-center">
     <b>poster 3</b>
  <p>
     <img src="images/Poster3.jpg" class="logo img-fluid" alt="poster3" id="logo" >
  </p>
  </div>

</footer>
 </div>
 

<!--</form>-->
 </div>
  </div>
</body>


</html>